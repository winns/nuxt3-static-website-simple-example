// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	ssr: true,
  	devtools: { enabled: false },


	hooks: {
		// Add dynamic pages that need to be prerendered
		async 'nitro:config'(nitroConfig) {
			if (nitroConfig.dev) return;

			const request = await fetch(`https://dummyjson.com/products`);
			const data = await request.json();
			const urls = data.products.map(o => `/products/${o.id}`);

			nitroConfig.prerender.routes.push(...urls);

			return;
		},
	},
})
